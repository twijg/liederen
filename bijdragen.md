## Hoe bijdragen?
Iedereen is vrij om bij te dragen aan deze collectie. Alle liederen worden in Markdown geformateerd, kijk ook eens naar andere liederen zodat overal
hetzelfde sjabloom gebruikt wordt. Als u opmerkingen heeft over het sjabloom en ideëen om het te verbetern, arzel dan zeker niet om die te delen.
Alle teksten zijn studentikoze liederen of kunnen dat worden, alle talen zijn toegestaan en de teksten zijn zonder veel moeite in te passen in een cantuscultuur.

Als herhalingsymbool kunt u "𝄇" gebruiken of ":|".

*Als u een tekst toevoegt, vergewis u er dan van dat u de rechten geeft om dat te doen en dat te tekst vrij te verspreiden is, bijvoorbeeld de tekst bevindt zich in het publiek domein of
wordt onder een vrije licentie uitgegeven. Meerbepaald moeten alle teksten compatibel zijn met de CC BY-SA 4.0, meer informatie hierover in LICENCE*