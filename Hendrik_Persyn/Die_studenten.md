## Die studenten
*Hendrik Persyn (1857 - 1933)*

1. Zes studenten in verlof,  
Watiwee Wetiwaa  
Ketsen heel hun Vlaanderen of,  
Wati-weti-waa;  
Drinken wel en rooken fel,  
Spelen vrij en vlaamsch gespel.  
Witi-Wati-Wetiwaa  
Wati-weti-waa!

2. Heil den baas van ‘Weetje-waar’!  
Watiwee enz.  
Zoo zoo roept ons vrome schaar,  
Wati enz.  
Want hij leende ons weer en goed  
Sporen aan den blauwen voet.  
Witi-wati-wetiwaa enz.


3. Overal de Vlagge in top,  
Heldere ooge, warme kop,  
En de strijdzang langs de reê:  
‘Vliegt de blauwvoet? storm op zee!’

4. Leefden wij nog honderd jaar,  
Nooit en rouwde 't onze schaar,  
Al ons doen voor 't Vlaamsche diet,  
't Gildeleven, 't Gildelied!
