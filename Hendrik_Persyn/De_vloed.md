## De vloed
*Hendrik Persyn (1857 - 1933)*

De Groote zee is weg, zucht gij,  
Maar 'k zeg u, dat is 't ebbetij;  
De zee zal keeren met den vloed,  
En vloed zal t' worden, - 'k doe dat goed -  
En hooge vloed - dat zult ge zien -  
Daar 't zoete volk zal leed om lijên.  
Dat volk dat, wis om ons beschaamd,  
Ons liefdevol onnooz'len naamt...  
.............  
.............

Ei! de ebbe is 't enden; 't ver geruisch  
Van baren ende windgedruisch  
Ronkt somber van uit volle zee  
En meldt den wand'laar op de ree  
Het magtig zwellen van den vloed  
Die Vlaanderen hopend trillen doet.
