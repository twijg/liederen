## Opwaards
*Hendrik Persyn (1857 - 1933)*

1. Wanneer een leger, strijden-zuchtig,  
Een ander onder de oogen treedt,  
Daar rijst van beide kanten duchtig  
Een muur van dreigend ijzer breed.  
En nauw heeft de trompet geschetterd  
En klinkt zij 't daverend luchtruim rond,  
Of iedere muur haar wederklettert  
En roepen duizende uit een mond:  
Voorwaards!

2. En als de strijd heeft aangevangen  
En de een muur op den anderen klotst,  
Dat van weêrkanten heele rangen  
Dood worden op den grond gebotst,  
Dan is er een die, voorwaards dringend,  
Door man- en peerden, onversaagd,  
Maar altijd doet het voorwaards klinken,  
Terwijl dat de andere stortend klaagt:  
Achterwaards!

3. Voor ons is 't strijd ook hier beneden;  
Het christen volk voert ook een vaan,  
En menig vijand treedt ons tegen  
En zal met ons den veldslag slaan:  
Maar, onder 't vechten en het strijden,  
Het rugwaards, 't voorwaards, 't raakt ons niet,  
Een roep, in 't treuren en verblijden,  
Maar altijd bonzend 't herte ontschiet:  
Opwaards!
