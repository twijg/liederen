## Die Waterlelie

*Hendrik Persyn (1857 - 1933)*

De blanke waterlelie  
Wijkt voor de zonnepracht,  
En met beloken boezem,  
Verbeidt zij droomend de nacht.  
De maan is haar vriendinne,  
Die wekt ze met heur licht,  
Voor haar ontsluiert zij vriendlijk  
Heur heerlijk blommengezicht.  
Zij lonkt vol liefde omhooge,  
Stom blaakt ze in 't geurend kleed;  
En staat en trilt in tranen  
Van louter liefdeleed.
