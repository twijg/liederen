## De rotse
*Hendrik Persyn (1857 - 1933)*

Al buld'ren heeft de woeste orkaan   
Het woelziek nat bestegen -  
Het monster schudt zijn blanke maan,  
't Komt grinzend aangedreven  
En grijpt met wilde krachten aan  
De Rotse.  
 
De wolkenreus met zwart gemoed  
Stapt aan; zijn brandende ooge  
Ontwaart de prooi. - In dolle woed'  
Hij grijpt zijn pijl en boge  
En bliksemt met een vlammenvloed  
De Rotse.  
 
Maar vruchtloos paart ge uw blinden haat;  
O Bliksem, Zee en Winden:  
Verdubbelt woede en kracht... en gaat!...   
Gij zult ze sterker vinden:  
Op 't eeuwig woord des Heeren staat  
