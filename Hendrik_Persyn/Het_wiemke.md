## Het wiemke
*Hendrik Persyn (1857 - 1933)*


1. Het wipte een wiemke in een weideken zoet,  
En 't hinkelde al op zijnen blauwen voet;

    *refrein?:  
Ei! 't was in de mei,  
Zo zei!  
Ei! 't was in de mei*


2. De mierkens kropen op 't gerzeken groen,  
En loechen zoo lustig om 't wiemkes doen,

3. Het rekte een krekel zijn mageren hals,  
En dons met de kobbe een blijde wals,

4. Het zonneke straalde lustig en hel  
Al schouwen over dat blij gespel,

5. Wel sprong er een geitje zoo spijtig in 't rond  
Omdat 't er geen fransche klaver en vond.

6. Maar 't kwam een daze daartoe gesneld,  
En ze stak er de trompe met groot geweld.

7. En met zijnen blauwen wippenden voet,  
Dreef 't wiemke dat geitje uit 't weideke zoet.

8. En 't hellemde alom nu ‘Vivan ons!’  
In Vlaanderen weiden geen fransquillons!
