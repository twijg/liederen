## Stilte
*Hendrik Persyn (1857 - 1933)*

Toen uit het vierig bloeden van de roode westerkimme,  
Aleer zij neerzinkt in het blakend diepen  
Der lucht, de rustige avondzon haar moederlike blikken  
Alover 't groene bonte veld laat weiden,  
En met heur lange stralen, lijk met uitgestrekte vingeren,  
Elk schepsel schijnt te willen dronke streelen,  
Het voelt hem al geneigd tot rust en vrede en stil genieten.  
..................  
..................
 
En ga ik dan de kalme velden in,  
Mijn ziel wordt kalm, en neuriet zachte liederen, lijk de zanger  
Wen hij een welbekende lied hoort zingen  
Niet laten kan van uit zijn gansche ziele meê te zingen;  
En stuwt mij drift, en pijnt mij wee, en ligt  
 
Het in mijn ziel als in een zee aan 't woelen en aan 't zieden,  
De stilte der natuur zal ondanks mij  
Den storm bedaren; want ik voele mij van haar als 't ware  
In de arms gegrepen, lijk een schreiend kind.  
Dat zijne moeder, angstig op haar minnend herte sluitend,  
Met kus en zoete woorden brengt tot zwijgen.  
Hoe woei de wind van gister naar mijn zin!  
Hoe stond zijn lied te akkoord met mijner ziel  
Onstuimig lied! Hoe horkte ik welgezind  
Hem zweven over 't blakke veld, al huilen  
De rijen hooge boomen rijden op,  
De vuisten slaan aan hunnen kalen kop,  
Ze schudden dat al hunne wortels kermden,  
En dat de bodem klachten kloeg als 't ware  
In barensnood.

O wen die binnenzee  
Ontstelt, die men de menschenziele noemt  
Wen langgevoede en langbedrogen driften  
Wen fierheid, spijt, verlangen, angst, mistrouwen,  
Vijandig op malkander botsend, 't herte  
Ten wilden kampe serren, en daar één  
Dier pijnelijke strijden baren, waar  
De ziel zoo machtig vele kan in lijden,  
Het doet eens deugd, als, buiten, de elementen,  
Ontbonden, zingen lijk de ziele zingt.
