## De zomernacht
*Hendrik Persyn (1857 - 1933)*

1. Daar kwam er ne reuze gewandeld,  
Gewandeld uit den bosch,  
Hij stapte deur de velden,  
Geheel in 't zwarte gedoscht.

2. Zijn hoofd zat in de wolken  
En pekzwart hong zijn hair,  
En onder zijn groeiende schaduwe  
Dook de aarde en de zee te gaâr.  

3. En schuddende kwam de reuze,  
En 't ruischte wijd en breed,  
En 't druppelde rondom de aarde,  
Van 't lange zwarte kleed.

4. Daar dronken, het hoofd omhooge,  
In wellust en wiegenden droom,  
De slapende blom en de weide  
En het veld en de zuchtende boom.

5. Zoo kwam er ne reuze gewandeld,  
Gewandeld al uit den bosch,  
Hij stapte deur de velden  
Geheel in 't zwarte gedoscht.
