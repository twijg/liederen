## Non proevalebunt
*Hendrik Persyn (1857 - 1933)*

Hoe storten al die eiken zwaar  
Ten gronde?  
Wie gooit en smijt hun trotsch geblaart  
In 't ronde?

Wie staat op hun verdelgde kruin  
In zege  
En heeft zijn troon op 't stuivend puin  
Geslegen?  
De zunne is door het wolkenheir  
Gebroken;  
Zij heeft heur breede viervlerk weer  
Ontloken,  
En hangt om 't hoofd van hem die vlocht  
Eene kroone.  
Geheel van vlamme en vier gewrocht  
Ten toone.
