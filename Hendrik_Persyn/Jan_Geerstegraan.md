## Jan Geerstegraan

*Hendrik Persyn (1857 - 1933)*  
*Op de wijs van Krambambouli*

1. Jan Geerstegraan, dat is de name  
Des oudsten aller graven van ons Vlaanderland;  
Geen keizer steeg zoo hoog in fame,  
Ofschoon dat Jan maar strijd en voert de schaal in d'hand.  
Ja Caesar was zijn tijdgenoot,  
En nog en is Graaf Jan niet dood  
Jan Graan, Jan Geerstegraan, Jan Geerstegraan.

2. Noch Spanjen, 't land van ‘warmen wine’,  
Alwaar men lustig levend met de bekers klinkt;  
Noch Duitschland waar men aan den Rhijne  
Den ‘Coelen’ uit de trossen perst, ‘und jauchzend trinkt’;  
Noch dit noch dat, wilt mij verstaan,  
En kan aan onzen Geerstegraan,  
Jan Graan, Jan Geerstegraan, Jan Geerstegraan!

3. Men roemt, ja, nog den eed'len ‘bruisschaart’  
Die, zegt men, in den lande van Champagne wast;  
Doch zwicht uw hoofd, of 't wordt 'ne ruisschaart,  
Van zoo gij wat te wale metter schale brast,  
En daarom laat de kruike gaan,  
Met 't peerlend bloed van Geerstegraan,  
Jan Graan, Jan Geerstegraan, Jan Geerstegraan!

4. Als 't male was op 't eiland Wulpen,  
Zat Wate met de Wikings aan het stormend strand,  
En dronken vrij veel versche schulpen,  
Al sagen van hun slagen tegen 't Wallenland;  
Ser Wate liet den hoorne gaan,  
En 't gong er: aan Jan Geerstegraan,  
Jan Graan, Jan Geerstegraan, Jan Geerstegraan.

5. Geen vromer held in Kerstenhede,  
Geen man lijk Grave Jan die Vlaanderen eere doet:  
In feest en spel, in strijd en vrede,  
Daar stroomt bij schaal- en bekersklank zijn edel bloed.  
Sa maten! laat ze nogmaals gaan,  
Voor onzen held Jan Geerstegraan,  
Jan Graan, Jan Geerstegraan, Jan Geerstegraan,