## 't Zevengesterre
*Guido Gezelle (1830-1899)*

Van Gent naar Geeraardsbergen,  
daar liggen zeven scherven,  
zeven scherven, al even blank ,  
langs nen wijden watergang:  
niemand die ze geraken kan,  
niemand die ze genaken kan:  
ra, wat zijn me die scherven dan?
