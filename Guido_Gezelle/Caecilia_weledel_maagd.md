## Caecilia, weledel maagd
*Guido Gezelle (1830-1899)*

Caecilia, weledel maagd,  
gelukkig die uw krone draagt  
en dien al't aards, hoe schoon en fijn,  
niet aards en doet maar hemesl zijn:  
dat doet uw kunste, en, triomfant,  
Gij aller kunsten krone spant!
