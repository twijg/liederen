## De Vlaamse tale is wonderzoet  
*Guido Gezelle (1830-1899)*

De Vlaamse tale is wonderzoet,  
voor die heur geen geweld en doet,  
maar rusten laat in 't herte, alwaar,  
ze onmondig leefde en sliep te gaar,  
tot dat ze, eens wakker, vrij en vrank,  
te monde uit, gaat heur vrijen gang!  
Wat verruwprachtig hoortoneel,  
wat zielverrukkend zingestreel!  
o Vlaamse tale, uw' kunste ontplooit,  
wanneer zij 't al  vol leven strooit  
en vol 't onzegbaar schoon zijn, dat,  
lijk wolken wierooks, welt  
uit uw zoet wierookvat!
