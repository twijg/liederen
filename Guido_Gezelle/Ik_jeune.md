## ik jeune mij daarin
*Guido Gezelle (1830-1899)*

Ik jeune mij daarin,  
ik jeune mij daaran,  
als ik een liedje mag dichten;  
ik jeune mij daarin,  
ik jeune mij daaran,  
als ik het liedeke kan.
