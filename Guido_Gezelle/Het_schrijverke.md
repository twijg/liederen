## Het schrijverke
### Gyrinus natans
*Guido Gezelle (1830-1899)*

O krinklende winklende waterding,  
Met 't zwarte kabotseken aan,  
Wat zien ik toch geren uw kopke flink  
Al schrijven op 't waterke gaan! 
 
Gij leeft en gij roert en gij loopt zoo snel,  
Al zie 'k u noch arrem noch been;  
Gij wendt en gij weet uwen weg zoo wel,  
Al zie 'k u geen ooge, geen één.  

Wat waart, of wat zijt, of wat zult gij zijn?  
Verklaar het en zeg het mij, toe!  
Wat zijt gij toch, blinkende knopke fijn,  
Dat nimmer van schrijven zijt moe?  

Gij loopt over 't spegelend water klaar,  
En 't water niet méér en verroert  
Dan of het een gladdige windtje waar,  
Dat stille over 't waterke voert.  

O schrijverkes, schrijverkes zegt mij dan, -  
Met twintigen zijt gij en meer,  
En is er geen een die 't mij zeggen kan: -  
Wat schrijft en wat schrijft gij zoo zeer?  

Gij schrijft, en 't en staat in het water niet,  
Gij schrijft, en 't is uit en 't is weg;  
Geen Christen en weet er wat dat bediedt:  
Och, schrijverke, zeg het mij, zeg!  

Zijn 't visselkes daar ge van schrijven moet?  
Zijn 't kruidekes daar ge van schrijft?  
Zijn 't keikes of bladtjes of blomkes zoet,  
Of 't water, waarop dat ge drijft?  

Zijn 't vogelkes, kwietlende klachtgepiep,  
Of is 'et het blauwe gewelf,  
Dat onder en boven u blinkt, zoo diep,  
Of is het u, schrijverken, zelf?  

En 't krinklende winklende waterding,  
Met 't zwarte kapoteken aan,  
Het stelde en het rechtte zijne oorkes flink,  
En 't bleef daar een stondeke staan:  

‘Wij schrijven.’ zoo sprak het, ‘al krinklen af  
Het gene onze Meester, weleer,  
Ons makend en leerend, te schrijven gaf,  
Eén lesse, niet min nochte meer;  

Wij schrijven, en kunt gij die lesse toch  
Niet lezen, en zijt gij zoo bot?  
Wij schrijven, herschrijven en schrijven nog,  
Den heiligen Name van God!’  
