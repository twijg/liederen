## Ergo Bibamus
*Johann Wolfgang von Goethe (1749 - 1832)*

1. Hier sind wir versammelt zu löblichem Thun,  
Drum Brüderchen Ergo Bibamus!  
Die Gläser sie klingen, Gespräche sie ruhn,  
Beherziget Ergo Bibamus!  
Das heist noch ein altes ein tüchtiges Wort,  
Es passet zum ersten und passet so fort,  
Und schallet ein Echo vom festlichen Ort,  
Ein herrliches Ergo Bibamus. 𝄇

2. Ich hatte mein freundliches Liebchen gesehn,  
Da dacht’ ich mir Ergo Bibamus.  
Und nahte mich traulich, da lies sie mich stehn,  
Ich half mir und dachte Bibamus.  
Und wenn sie versöhnet Euch herzet und küsst,  
Und wenn ihr das Herzen und Küssen vermisst,  
So bleibet nur, bis ihr was besseres wisst,  
Beym tröstlichen Ergo Bibamus. 𝄇

3. Mich ruft das Geschick von den Freunden hinweg  
Ihr Redlichen Ergo Bibamus.  
Ich scheide von hinnen mit leichtem Gepäck,  
Drum doppeltes Ergo Bibamus.  
Und was auch der Filz von dem Leibe sich schmorgt  
So bleibt für den Heitren doch immer gesorgt  
Weil immer dem Frohen der Frölige borgt.  
un Brüderchen Ergo Bibamus. 𝄇

4. Was sollen wir sagen zum heutigen Tag?  
Ich dächte nur Ergo Bibamus.  
Es ist nun einmal von besonderem Schlag,  
Drum immer aufs neue Bibamus.  
Er führet die Freunde durchs offene Thor,  
Es glänzen die Wolcken, es theilt sich der Flor,  
Da leuchtet ein Bildchen ein göttliches vor  
Wir klingen und singen Bibamus. 𝄇
